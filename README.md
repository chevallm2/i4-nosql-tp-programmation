# Notes

## Équipe
- Gabriel Fruhauf <<gabriel.fruhauf@epsi.fr>>
- Maxime Chevallier-Pichon <<m.chevallierpichon@epsi.fr>>

## Lancement

- Sur le serveur Redis, exécuter cette commande `CONFIG SET protected-mode no` afin que le serveur soit joignable par l'application ou lancer son serveur comme ceci : `redis-server --protected-mode no` (si votre serveur Redis n'est pas sur la même machine qui héberge l'application)
    - Une solution plus durable serait de mettre directement cette expression dans le fichier de configuration du serveur Redis
- Lancer `npm i` dans le répertoire afin d'être sûr d'avoir les packages d'installés
- Dans le fichier `config.json` valoriser la valeur `URL_REDIS` avec l'adresse IP de la machine hébergeant votre serveur Redis
- Lancer `npm start`
- L'application est normalement disponible sur le port 12345

## Utilisation
Aussi disponible sur l'index du projet : <http://localhost:12345/>
### GET /notes

- Aucun paramètre
- Réponse : application/json
    - 200 : Array<Note>
    - 404 : Aucune note

### POST /notes

- Paramètre : application/json
```json
{
    "texte": "ici le texte de votre note",
    "auteur": "ici le nom de l'auteur de la note"
}
```
- Réponse :
    - 201 : Created, ID de la note (disponible à l'URI indiquée dans le header 'HTTP Location')
    - 400 : Bad request

### GET /notes/:id

- Paramètre :
    - id: un entier correspondant à l'ID de la note
- Réponse : application/json
    - 200 : OK, une Note
    - 400 : Bad request
    - 404 : Aucune note avec cet ID

### DELETE /notes/:id

- Paramètre :
    - id : un entier correspondant à l'ID de la note
- Réponse : application/json
    - 200 : OK
    - 400 : Bad request

### GET /subscribe/:auteur

- Paramètre :
    - auteur: une chaine de caractère représentant le nom de l'auteur de la note (case sensitive)
- Réponse : application/json
    - 200 : OK, Array<Note>
    - 404 : Aucune note pour cet auteur

## Modèle

### Note

| Propriété | Type   |
| --------- | ------ |
| texte     | string |
| auteur    | string |
| date      | date   |

## Réponses aux questions

Pour plus de facilité, voici où se situe le code de chaque question :

- Question 1 : Création d'une note
    - `./routes/notes.js:37`
- Question 2 : Récupération des notes
    - `./routes/notes.js:13`
- Question 3 : Récupération d'une note
    - `./routes/notes.js:79`
- Bonus 1 : Supprimer une note
    - `./routes/notes.js:97`
- Bonus 2 : Note avec auteur et date de création
    - `./routes/notes.js:37`
    - Utilisation de `JSON.stringify()` pour l'envoi à Redis
- Bonus 3 : Système d'abonnement à un auteur
    - `./routes/subscribe.js:13`
    - Utilisation du système publish/subscribe de Redis