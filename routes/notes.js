const express = require('express')
const router = express.Router()
const fs = require('fs')

const redis = require('redis');
const config = JSON.parse(fs.readFileSync('config.json', 'utf-8'));

const rc = redis.createClient({host: config.URL_REDIS});

rc.on("error", (err) => { console.log("Redis error : ", err )});

// GET /notes
router.get('/', function(req, res, next) {
  // On récupère toutes les notes
  rc.lrange("notes", 0, -1, (err, reply) => {
    // On les récupère au format JSON
    const notes = reply.map( note => { return JSON.parse(note) })
    // Si on a au moins 1 élément
    if(notes.length > 0) {
      res.send(notes) // On les renvoie
    } else {
      res.sendStatus(404) // Sinon on renvoie le status 404
    }
  })
})

// POST /notes
// Code original avant enrichissement des questions bonus
/*router.post('/', (req, res, next) => {
    const note = req.body
    rc.lpush("notes", note, (err, reply) => {
        res.setHeader("Location", `http://localhost:12345/notes/${reply}`)
        res.status(201).send(reply.toString()) // On renvoyais l'indice de la chaine dans la liste
    })
})*/

router.post('/', (req, res, next) => {
  
  if(!req.body.texte || !req.body.auteur) {
    // Si la requete n'as pas les arguments attendus
    res.sendStatus(400) // On lance une erreur 404
  } else {
    // On récupères toutes les notes
    rc.lrange("notes", 0, -1, (err, reply) => {
      // On les récupère au format JSON
      const notes = reply.map( note => { return JSON.parse(note) })
      
      // Par défault l'ID sera set à 1
      let ID = 1
      // Si on a au moins un élément
      if(notes.length > 0) {
        // On récupère l'ID du dernier élément dans la liste et on ajoute 1
        ID = notes[0]._id + 1
      }
        
      // On crée la note
      const note = {
        _id: ID,
        texte: req.body.texte,
        auteur: req.body.auteur,
        date: new Date().toString() // La date time du moment
      }

      // On ajoute l'élément dans la liste
      rc.lpush("notes", JSON.stringify(note), (err, reply) => {
        rc.publish('notes_publish', JSON.stringify(note))
        // On ajoute l'URL de la note dans le header HTTP Location
        res.setHeader("Location", `http://localhost:12345/notes/${ID}`)
        // On renvoie un status 201 avec comme réponse l'ID de la note
        res.status(201).send(ID.toString())
      })
    })
    
    
  }
});

// GET /notes/id
router.get('/:id', (req, res, next) => {
  // On récupère l'ID depuis l'URL
  const id = req.params.id
  console.log("On veux la note n° ", id);
  
  // On récupère toutes les notes
  rc.lrange('notes', 0, -1, (err, reply) => {
    // On les récupère au format JSON
    const notes = reply.map( note => { return JSON.parse(note) })
    // On regarde si un note comporte notre ID
    // Le résultat est un tableau mais l'on veux juste un objet
    const note = notes.filter( n => n._id == id)[0] 
    // Si oui, on l'envoie, sinon on envoie un status 404
    note ? res.send(note) : res.sendStatus(404);
  })
});

// DELETE /notes/id
router.delete('/:id', (req, res, next) => {
  // On récupère l'ID depuis l'URL
  const idToDelete = req.params.id
  // On récupère toutes les notes
  rc.lrange("notes", 0, -1, (err, reply) => {
    // On les récupère au format JSON
    const notes = reply.map( note => { return JSON.parse(note) })
    // On regarde si un note comporte notre ID
    const note = notes.filter( note => note._id == idToDelete)[0]
    // Si on ne trouve pas de note avec cet ID
    if(!note) { res.sendStatus(404) } // On envoie un status 404
    // On transforme l'objet en chaine de caractère
    const elementAEnlever = JSON.stringify(note)
    // Pour le supprimer de la liste
    rc.lrem("notes", 1, elementAEnlever, (err, reply) => {
      res.sendStatus(200)
    })
  })
});



module.exports = router
