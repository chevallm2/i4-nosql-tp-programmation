const express = require('express')
const router = express.Router()
const fs = require('fs')

const redis = require('redis')
const config = JSON.parse(fs.readFileSync('config.json', 'utf-8'))

const rc = redis.createClient({host: config.URL_REDIS})
const subscriber = redis.createClient({host: config.URL_REDIS})

rc.on("error", (err) => { console.log("Redis error : ", err )})

router.get('/:auteur', (req, res, next) => {

  const auteur = req.params.auteur;

  rc.lrange("notes", 0, -1, (err, reply) => {
    const notes = reply.map( note => { return JSON.parse(note) })
    const notesAuteur = notes.filter(note => note.auteur == auteur)

    subscriber.on("message", (channel, message) => {
      const noteRecu = JSON.parse(message)
      //if(noteRecu.auteur == auteur) {
        notesAuteur.push(noteRecu)
        res.send(notesAuteur)
      //}
    })
  })
})

subscriber.subscribe("notes_publish")




module.exports = router
